const express = require('express')
const api = express()
const port = 8080
const cors = require('cors')

api.use(cors())

api.listen(port,()=> console.log('Hello world'))

api.get('/', (request, response)=>{
  response.send('Hello world')
})
